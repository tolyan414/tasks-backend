const mongoose = require('mongoose')

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})

const db = mongoose.connection
db.once('open', () => console.log('connected to mongo db at', process.env.MONGO_URI) )

const tasksSchema = mongoose.Schema({
  username: { type: String, index: true, required: true },
  email: { type: String, index: true, required: true },
  status: { type: Number, index: true, default: 0 },
  text: { type: String, required: true }
})
const tasks = mongoose.model('tasks', tasksSchema)

exports.models = { tasks }
