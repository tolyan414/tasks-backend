require('dotenv').config({ path: '../.env' })
require('log-timestamp')
const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const cors = require('cors')
const tasks = require('./routes/tasks')

app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cors())
app.use('/tasks', tasks)

app.listen(port, () => {
  console.log(`tasks-backend listening at port ${port}`)
})
