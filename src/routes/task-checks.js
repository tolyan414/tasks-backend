function checkDeveloperParam (req, res, next) {
  if (req.query.developer) {
    next()
  } else {
    res.status(500).json({
      status: "error",
      message: "Не передано имя разработчика"
    })
  }
}

function checkTaskCreateParams(req, res, next) {
  const message = {}
  if (!req.body.username) {
    message.username = "Поле является обязательным для заполнения"
  }
  if (!req.body.text) {
    message.text = "Поле является обязательным для заполнения"
  }
  if (!validateEmail(req.body.email)) {
    message.email = "Неверный email"
  }

  if (Object.keys(message).length) {
    res.status(500).json({
      status: 'error',
      message
    })
  } else {
    next()
  }
}

function validateEmail(email) {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

function checkTasksListQuery(req, res, next) {
  const sortFields = ['id', 'username', 'email', 'status']
  const message = {}
  const rq = req.query
  if (rq.page && isNaN(rq.page)) {
    message.page = 'Неверное значение'
  }
  if (rq.sort_field && !sortFields.includes(rq.sort_field)) {
    message.sort_field = "Неверное значение"
  }
  if (rq.sort_direction && !['asc', 'desc'].includes(rq.sort_direction)) {
    message.sort_direction = "Неверное значение"
  }

  if (Object.keys(message).length) {
    res.status(500).json({
      status: 'error',
      message
    })
  } else {
    next()
  }
}

module.exports = { checkDeveloperParam, checkTaskCreateParams, checkTasksListQuery }
