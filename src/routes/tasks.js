const express = require('express')
const router = express.Router()
const { models } = require('../models')
const {
  checkDeveloperParam,
  checkTaskCreateParams,
  checkTasksListQuery
} = require('./task-checks')

router.get('/', checkDeveloperParam, checkTasksListQuery, async (req, res) => {
  try {
    const page = +req.query.page || 0
    const sortField = req.query.sort_filed || 'id'
    const sortDirection = req.query.sort_direction || 'asc'
    const sortObj = { [sortField === 'id' ? '_id': sortField]: sortDirection }
    const rpp = +process.env.ROWS_PER_PAGE || 5

    const [total_task_count, tasks ] = await Promise.all([
      models.tasks.countDocuments(),
      models.tasks.find()
        .select("-__v")
        .sort(sortObj)
        .skip(rpp * page)
        .limit(rpp)
    ])

    res.json({
      status: 'ok',
      tasks: tasks.map((t) => {
        const { _id, username, email, status, text } = t
        return { id: _id, username, email, status, text }
      }),
      total_task_count
    })

  } catch (e) {
    console.error(e)
    res.status(500).json({
      status: 'error',
      message: e
    })
  }
})

router.post('/create', checkTaskCreateParams, async (req, res) => {
  try {
    const { _id, status, username, email, text } = await models.tasks.create(req.body)
    res.json({
      status: 'ok',
      message: { id: _id, status, username, email, text }
    })

  } catch (e) {
    console.error(e)
    res.status(500).json({
      status: 'error',
      message: e
    })
  }
})

module.exports = router
